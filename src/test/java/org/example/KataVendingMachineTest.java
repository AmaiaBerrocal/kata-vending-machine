package org.example;

import org.example.utils.Printer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.example.CoinsType.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class KataVendingMachineTest {

    Machine sut;
    Printer printer;
    Display display;

    @BeforeEach
    public void setUp() {
        printer = mock(Printer.class);
        display = new Display(printer);
    }

    @Test
    public void should_accept_coins() {
        //given
        sut = new Machine(Map.of(), display);
        //when
        sut.acceptCoins(NICKLE.getSize(), NICKLE.getWeight());
        //then
        assertThat(sut.getAmount()).isEqualTo(5);
    }

    @Test
    public void should_diff_between_valid_or_invalid_coin() {
        //given
        sut = new Machine(Map.of(), display);
        Double size = 20.21;
        Integer weight = 5100;
        //when
        sut.acceptCoins(size, weight);
        //then
        assertThat(sut.getAmount()).isEqualTo(0);
    }

    @Test
    public void should_increase_the_amount() {
        //given
        sut = new Machine(Map.of(), display);
        Double size4 = 20.21;
        Integer weight4 = 5000;
        //when
        sut.acceptCoins(NICKLE.getSize(), NICKLE.getWeight());
        sut.acceptCoins(QUARTER.getSize(), QUARTER.getWeight());
        sut.acceptCoins(NICKLE.getSize(), NICKLE.getWeight());
        sut.acceptCoins(size4, weight4);
        //then
        assertThat(sut.getAmount()).isEqualTo(35);
    }

    @Test
    public void should_return_insert_coin_if_amount_is_zero() {
        //given
        sut = new Machine(Map.of(), display);
        //when
        display.printInsertCoin();
        //then
        verify(printer).println("INSERT COIN");
    }

    @Test
    public void should_return_current_amount_if_amount_is_not_zero() {
        //given
        sut = new Machine(Map.of(), display);
        //when
        sut.acceptCoins(NICKLE.getSize(), NICKLE.getWeight());
        //then
        verify(printer).println(NICKLE.getValue().toString());
    }

    @Test
    public void rejected_coins_should_be_placed_in_the_coin_return() {
        //given
        sut = new Machine(Map.of(), display);
        //when
        sut.acceptCoins(NICKLE.getSize(), NICKLE.getWeight());
        sut.acceptCoins(PENNIE.getSize(), PENNIE.getWeight());
        sut.acceptCoins(PENNIE.getSize(), PENNIE.getWeight());
        //then
        assertThat(sut.getCoinReturn().get(0)).isEqualTo(PENNIE);
        assertThat(sut.getCoinReturn().get(1)).isEqualTo(PENNIE);
    }

    @Test
    public void should_return_the_stock_of_a_product() {
        //given
        sut = new Machine(Map.of("cola", List.of(new Product("cola", 100))), display);
        //when
        //then
        assertThat(sut.getStock("cola")).isEqualTo(1);
    }

    @Test
    public void should_return_cola_if_cola_is_pressed() {
        //given
        Product cola = new Product("cola", 100);
        sut = new Machine(Map.of("cola", new ArrayList<>(List.of(cola))), display);
        //when
        String selectedProduct = "cola";
        //then
        assertThat(sut.dispenseProduct(selectedProduct)).isEqualTo(cola);
    }

    @Test
    public void the_display_should_display_thanks_message_if_product_is_dispensed() {
        //given
        Product cola = new Product("cola", 100);
        sut = new Machine(Map.of("cola", new ArrayList<>(List.of(cola))), display);
        //when
        String selectedProduct = "cola";
        sut.dispenseProduct(selectedProduct);
        //then
        verify(printer).println("THANK YOU!");
    }

    @Test
    public void should_subtract_the_product_price_of_the_amount() {
        //given
        Product cola = new Product("cola", 100);
        sut = new Machine(Map.of("cola", new ArrayList<>(List.of(cola))), display);
        sut.acceptCoins(QUARTER.getSize(), QUARTER.getWeight());
        sut.acceptCoins(QUARTER.getSize(), QUARTER.getWeight());
        sut.acceptCoins(QUARTER.getSize(), QUARTER.getWeight());
        sut.acceptCoins(QUARTER.getSize(), QUARTER.getWeight());
        //when
        String selectedProduct = "cola";
        sut.dispenseProduct(selectedProduct);
        //then
        assertThat(sut.getAmount()).isEqualTo(0);
    }

    @Test
    public void should_give_the_refund() {
        //given
        Product cola = new Product("cola", 100);
        sut = new Machine(Map.of("cola", new ArrayList<>(List.of(cola))), display);
        sut.acceptCoins(QUARTER.getSize(), QUARTER.getWeight());
        sut.acceptCoins(QUARTER.getSize(), QUARTER.getWeight());
        sut.acceptCoins(QUARTER.getSize(), QUARTER.getWeight());
        sut.acceptCoins(QUARTER.getSize(), QUARTER.getWeight());
        sut.acceptCoins(NICKLE.getSize(), NICKLE.getWeight());
        //when
        List<CoinsType> resp = sut.calculateRefund(cola.getPrice());
        //then
        assertThat(resp.get(0)).isEqualTo(NICKLE);
    }

    @Test
    public void should_display_sold_out_if_there_arent_stock() {
        ///given
        sut = new Machine(Map.of("cola", new ArrayList<>(List.of())), display);
        //when
        String selectedProduct = "cola";
        sut.chooseProduct(selectedProduct);
        //then
        verify(printer).println("SOLD OUT");
    }
}
