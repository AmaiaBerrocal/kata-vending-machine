package org.example;

import org.example.utils.Printer;

import java.text.DecimalFormat;
import java.util.List;
import java.util.SimpleTimeZone;

public class Display {
    Printer printer;

    public Display(Printer printer) {
        this.printer = printer;
    }
    public void printThankYou() {
        printer.println("THANK YOU!");
    }

    public void printInsertCoin() {
        printer.println("INSERT COIN");
    }

    public void printProductPrice(Integer price) {
        DecimalFormat decimalFormat = new DecimalFormat("#,##");
        printer.println(decimalFormat.format(price));
    }

    public void printAmount(Integer amount) {
        DecimalFormat decimalFormat = new DecimalFormat("#,##");
        printer.println(decimalFormat.format(amount));
    }

    public void printSoldOut() {
        printer.println("SOLD OUT");
    }
}
