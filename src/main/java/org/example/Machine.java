package org.example;

import java.util.*;

public class Machine {
    List<CoinsType> validCoinTypes = List.of(CoinsType.NICKLE, CoinsType.DIME, CoinsType.QUARTER);

    List<CoinsType> coinReturnList = new ArrayList<>();
    Integer amount = 0;
    Map<String, List<Product>> products;
    Display display;

    public Machine(Map<String, List<Product>> products, Display display) {
        this.products = products;
        this.display = display;
    }

    public void acceptCoins(Double size, Integer weight) {
        Optional<CoinsType> coin = findCoinType(size, weight);
        if (coin.isEmpty()) {
            coinReturnList.add(CoinsType.PENNIE);
            return;
        }
        amount += coin.get().getValue();
        display.printAmount(amount);
    }

    private Optional<CoinsType> findCoinType(Double size, Integer weight) {
        for (CoinsType validCoinType : validCoinTypes) {
            if (areMeasuresOfCoinType(size, weight, validCoinType)) {
                return Optional.of(validCoinType);
            }
        }
        return Optional.empty();
    }

    private static boolean areMeasuresOfCoinType(Double size, Integer weight, CoinsType validCoin) {
        return validCoin.getSize().equals(size) && validCoin.getWeight().equals(weight);
    }

    public Integer getAmount() {
        return amount;
    }

    public List<CoinsType> getCoinReturn() {
        return coinReturnList;
    }

    public Integer getStock(String product) {
        return products.get(product).size();
    }

    public Optional<Product> chooseProduct(String selectedProduct) {
        if (getStock(selectedProduct)>0) {
            return Optional.of(dispenseProduct(selectedProduct));
        }
        display.printSoldOut();
        return Optional.empty();
    }

    public Product dispenseProduct(String selectedProduct) {
        Product product = products.get(selectedProduct).remove(0);
        display.printThankYou();
        calculateRefund(product.getPrice());

        return product;
    }

    public List<CoinsType> calculateRefund(Integer price) {
        amount -= price;
        Integer rest = 0;
        if (!amount.equals(rest)) {
            rest = amount;
        }
        giveExchangeCoins(rest);
        display.printInsertCoin();
        return coinReturnList;
    }

    private void giveExchangeCoins(Integer rest) {
        while (rest >= 25) {
            coinReturnList.add(CoinsType.QUARTER);
            rest -=25;
        }
        while (rest >= 10) {
            coinReturnList.add(CoinsType.DIME);
            rest -=10;
        }
        while (rest >= 5) {
            coinReturnList.add(CoinsType.NICKLE);
            rest -=5;
        }
    }
}
