package org.example;

import java.util.ArrayList;
import java.util.List;

public class CoinReturn {
    List<CoinsType> coinReturnList = new ArrayList<>();

    public List<CoinsType> getCoinReturn() {
        return coinReturnList;
    }

    public void setCoinReturn(List<CoinsType> coinReturnList) {
        this.coinReturnList = coinReturnList;
    }

}
