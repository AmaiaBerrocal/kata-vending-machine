package org.example;

public enum CoinsType {
    PENNIE(19.05, 2500, 1),
    NICKLE(21.21, 5000, 5),
    DIME(17.91, 2268, 10),
    QUARTER(24.26, 5670, 25);

    private final Double size;
    private final Integer weight;
    private final Integer value;


    CoinsType(Double size, Integer weight, Integer value) {
        this.size = size;
        this.weight = weight;
        this.value = value;
    }

    public Double getSize() {
        return size;
    }

    public Integer getWeight() {
        return weight;
    }

    public Integer getValue() {
        return value;
    }
}
