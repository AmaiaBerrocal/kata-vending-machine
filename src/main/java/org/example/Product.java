package org.example;

public class Product {
    private String type;
    private Integer price;

    public Product(String type, Integer price) {
        this.type = type;
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public Integer getPrice() {
        return price;
    }
}
